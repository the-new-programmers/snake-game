let htmlBordElement;
const bord = [];
const snake = [];
const bordWidth = 40;

window.addEventListener('load', () => {

// create the js bord object
    for (let i = 0; i < bordWidth; i++) {
        bord[i] = [];
        for (let j = 0; j < bordWidth; j++) {
            bord[i].push({});
        }
    }

// add the snake to the bord
    bord[bordWidth / 2]      [bordWidth / 2]  = {val: 'snake'};
    bord[(bordWidth / 2) - 1][bordWidth / 2]  = {val: 'snake'};
    bord[(bordWidth / 2) - 2][bordWidth / 2]  = {val: 'snake'};

    snake.push( {x: bordWidth / 2,          y: bordWidth / 2} );
    snake.push( {x: (bordWidth / 2) - 1,    y: bordWidth / 2} );
    snake.push( {x: (bordWidth / 2) - 2,    y: bordWidth / 2} );


    htmlBordElement = document.querySelector('#bord');

    drawHtmlBord();
});


function drawHtmlBord() {
    // reset html bord!
    htmlBordElement.innerHTML = '';

    // create html bord
    for (const keyRow in bord) {

        let rowHtmlElement = document.createElement("div");
        rowHtmlElement.setAttribute("id", `row-${keyRow}`);
        rowHtmlElement.setAttribute("class", `row`);
        htmlBordElement.appendChild(rowHtmlElement);

        for (const keyCell in bord[keyRow]) {
            let cellHtmlElement = document.createElement("span");
            cellHtmlElement.setAttribute("id", `row-${keyRow}-cell-${keyCell}`);
            cellHtmlElement.setAttribute("class", `cell`);

            if (bord[keyRow][keyCell].val === 'snake') {
                cellHtmlElement.setAttribute("class", `cell snake`);
            }

            rowHtmlElement.appendChild(cellHtmlElement);
        }
    }
}
