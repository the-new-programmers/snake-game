
const moveSnakeInterval = setInterval(() => {
    move();
}, 100);

let snakeDir;


document.addEventListener('keydown', (e) => {
    if (e.code === "ArrowUp" && snakeDir !="down") {
        console.log("up");
        bord[snake[0].x][snake[0].y] = {val: ''};
        snake.shift();
        new_x = snake[snake.length - 1].x - 1;
        new_y = snake[snake.length - 1].y;
        snake.push({
            x: new_x,
            y: new_y
        });
        bord[new_x][new_y] = {val: 'snake'};
        drawHtmlBord(); 
        snakeDir = "up";
    }
    else if (e.code === "ArrowDown" && snakeDir !="up") {
        console.log("down");
        bord[snake[0].x][snake[0].y] = {val: ''};
        snake.shift();
        new_x = snake[snake.length - 1].x + 1;
        new_y = snake[snake.length - 1].y;
        snake.push({
            x: new_x,
            y: new_y
        });
        bord[new_x][new_y] = {val: 'snake'};
        drawHtmlBord();
        snakeDir = "down"; 
    }
    else if (e.code === "ArrowLeft" && snakeDir != "right") {
        console.log("left");
        bord[snake[0].x][snake[0].y] = {val: ''};
        snake.shift();
        new_x = snake[snake.length - 1].x;
        new_y = snake[snake.length - 1].y - 1;
        snake.push({
            x: new_x,
            y: new_y
        });
        bord[new_x][new_y] = {val: 'snake'};
        drawHtmlBord();
        snakeDir = "left"; 
    }
    else if (e.code === "ArrowRight" && snakeDir !="left") {
        console.log("right");
        bord[snake[0].x][snake[0].y] = {val: ''};
        snake.shift();
        new_x = snake[snake.length - 1].x;
        new_y = snake[snake.length - 1].y + 1;
        snake.push({
            x: new_x,
            y: new_y
        });
        bord[new_x][new_y] = {val: 'snake'};
        drawHtmlBord();
        snakeDir = "right"; 
    }
});


function move() {
    // update snake and bord
    bord[snake[0].x][snake[0].y] = {val: ''};
    snake.shift();

    // calculate direction snake
    let new_x;
    let new_y;
    switch (true) {
        case snake[snake.length - 1].x < snake[snake.length - 2].x:
            // up
            new_x = snake[snake.length - 1].x - 1;
            new_y = snake[snake.length - 1].y;
            break;
        case snake[snake.length - 1].x > snake[snake.length - 2].x:
            // down
            new_x = snake[snake.length - 1].x + 1;
            new_y = snake[snake.length - 1].y;
            break;
        case snake[snake.length - 1].y < snake[snake.length - 2].y:
            // left
            new_x = snake[snake.length - 1].x;
            new_y = snake[snake.length - 1].y - 1;
            break;
        default:
            // right
            new_x = snake[snake.length - 1].x;
            new_y = snake[snake.length - 1].y + 1;
            break;
    }


    // update snake and bord
    snake.push({
        x: new_x,
        y: new_y
    });

    // check if the user is failed
    if (
        snake[snake.length - 1].x < 0
        || snake[snake.length - 1].x > bordWidth
        || snake[snake.length - 1].y < 0
        || snake[snake.length - 1].y > bordWidth
    ) {
        clearInterval(moveSnakeInterval);
        alert('נפסלת יא לוזר');
    }
    else {
        bord[new_x][new_y] = {val: 'snake'};

        // draw HTML
        drawHtmlBord();
    }

}




